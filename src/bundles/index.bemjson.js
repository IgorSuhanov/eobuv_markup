module.exports = {
  block: 'page',
  title: 'Пустая странциа',
  content: [
    require('./common/header.bemjson.js'),
      {
          block: 'main',
          content: [{
              mix: {
                  block: 'container'
              },
              content: [{
                  block: 'features-list',
                  tag: 'ul',
                  content: [{
                      elem: 'item',
                      tag: 'li',
                      content: [{
                          block: 'icon-feature-1',
                          tag: 'i'
                      }, 'Большие размеры всегда в наличии']
                  }, {
                      elem: 'item',
                      tag: 'li',
                      content: [{
                          block: 'icon-feature-2',
                          tag: 'i'
                      }, 'Оффициальный диллер Tamaris и Riker']
                  }, {
                      elem: 'item',
                      tag: 'li',
                      content: [{
                          block: 'icon-feature-3',
                          tag: 'i'
                      }, 'Кожаная обувь из Германии']
                  }, {
                      elem: 'item',
                      tag: 'li',
                      content: [{
                          block: 'icon-feature-4',
                          tag: 'i'
                      }, 'Накопительная система скидок']
                  }, {
                      elem: 'item',
                      tag: 'li',
                      content: [{
                          block: 'icon-feature-5',
                          tag: 'i'
                      }, 'Межсезонные распрадажи и акции']
                  }]
              }]
          }]
      }, {
          block: 'main-slider',
          mix: {
              block: 'swiper-container'
          },
          content: [{
              mix: {
                  block: 'swiper-wrapper'
              },
              content: [{
                  mix: {
                      block: 'swiper-slide'
                  },
                  content: [{
                      tag: 'img',
                      attrs: {
                          src: 'images/slider/image-1.jpg',
                          alt: 'image-1'
                      }
                  }]
              }, {
                  mix: {
                      block: 'swiper-slide'
                  },
                  content: [{
                      tag: 'img',
                      attrs: {
                          src: 'images/slider/image-2.jpg',
                          alt: 'image-2'
                      }
                  }]
              }, {
                  mix: {
                      block: 'swiper-slide'
                  },
                  content: [{
                      tag: 'img',
                      attrs: {
                          src: 'images/slider/image-3.jpg',
                          alt: 'image-3'
                      }
                  }]
              }]
          }, {
              block: 'swiper-pagination'
          }, {
              block: 'swiper-button-next'
          }, {
              block: 'swiper-button-prev'
          }]
      }, {
          block: 'main',
          tag: 'main',
          content: [{
              mix: {
                  block: 'container'
              },
              content: []
          }]
      },
    require('./common/footer.bemjson.js')
  ]
};
