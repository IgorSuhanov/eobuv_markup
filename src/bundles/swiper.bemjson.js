module.exports = {
    block: 'page',
    title: 'Текстовая страница',
    content: [
        {
            tag: 'header',
            content: 'header'
        },
        {
            tag: 'main',
            content: [{
                block: 'swiper-container',
                content: [{
                    block: 'swiper-wrapper',
                    content: [{
                        block: 'swiper-slide',
                        mix: {
                            block: 'menu'
                        },
                        content: 'Menu slide'
                    }, {
                        block: 'swiper-slide',
                        mix: {
                            block: 'content'
                        },
                        content: [{
                            block: 'menu-button'
                        }]
                    }]
                }]
            }]
        },
        {
            tag: 'footer',
            content: 'footer'
        }
    ]
};
