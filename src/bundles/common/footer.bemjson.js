module.exports = [
    {
        block: 'footer', content: [
        {
            mix: {block: 'container'}, content: [{
            mix: {
                block: 'row'
            },
            content: [{
                mix: [{
                    block: 'col-xl-2'
                }, {
                    block: 'col-lg-2'
                }, {
                    block: 'col-md-4'
                }, {
                    block: 'col-12'
                }],
                content: [{
                    elem: 'heading',
                    tag: 'h6',
                    content: 'Каталог'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'Женская обувь'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'Мужская обувь'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'Аксессуары'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'Бренды'
                }]
            }, {
                mix: [{
                    block: 'col-xl-2'
                }, {
                    block: 'col-lg-2'
                }, {
                    block: 'col-md-4'
                }, {
                    block: 'col-12'
                }],
                content: [{
                    elem: 'heading',
                    tag: 'h6',
                    content: 'Компания'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'О компании'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'Магазины'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'Вакансии'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'Благотворительность'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'Отзывы работников'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'Отзывы покупателей'
                }]
            }, {
                mix: [{
                    block: 'col-xl-2'
                }, {
                    block: 'col-lg-2'
                }, {
                    block: 'col-md-4'
                }, {
                    block: 'col-12'
                }],
                content: [{
                    elem: 'heading',
                    tag: 'h6',
                    content: 'Клиентам'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'Доставка'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'Возврат'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'Вопрос/Ответ'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'Сотрудничество'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'Акции'
                }, {
                    elem: 'link',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'Новости'
                }]
            }, {
                mix: [{
                    block: 'col-xl-3'
                }, {
                    block: 'col-lg-3'
                }, {
                    block: 'order-lg-4'
                }, {
                    block: 'mt-lg-0'
                }, {
                    block: 'col-md-6'
                }, {
                    block: 'order-md-5'
                }, {
                    block: 'mt-md-5'
                }, {
                    block: 'col-sm-12'
                }, {
                    block: 'mb-5'
                }, {
                    block: 'col-12'
                }],
                content: [{
                    elem: 'heading',
                    tag: 'h6',
                    content: 'связаться с нами'
                }, {
                    block: 'phone',
                    tag: 'a',
                    attrs: {
                        href: 'tel:88001000324'
                    },
                    content: [{
                        mix: {
                            block: 'icon-phone'
                        },
                        tag: 'i'
                    }, '88001000324', {
                        elem: 'title',
                        content: 'Звонок по России бесплатный'
                    }]
                }, {
                    block: 'mail',
                    tag: 'a',
                    attrs: {
                        href: 'mailto:info@eobuv.ru'
                    },
                    content: [{
                        mix: {
                            block: 'icon-mail'
                        },
                        tag: 'i'
                    }, 'info@eobuv.ru']
                }, {
                    block: 'btn',
                    tag: 'button',
                    mix: [{
                        block: 'btn-danger'
                    }, {
                        block: 'btn-lg'
                    }],
                    content: 'Заказать звонок'
                }]
            }, {
                mix: [{
                    block: 'col-xl-3'
                }, {
                    block: 'col-lg-3'
                }, {
                    block: 'order-lg-5'
                }, {
                    block: 'mt-lg-0'
                }, {
                    block: 'col-md-6'
                }, {
                    block: 'order-md-4'
                }, {
                    block: 'mt-md-5'
                }, {
                    block: 'col-sm-12'
                }, {
                    block: 'col-12'
                }],
                content: [{
                    block: 'logo',
                    tag: 'img',
                    attrs: {
                        src: 'images/logo.png',
                        alt: 'logo'
                    }
                }, {
                    mix: {
                        block: 'd-block'
                    },
                    tag: 'small',
                    content: 'обувь европейских производителей'
                }, {
                    block: 'social',
                    content: [{
                        elem: 'title',
                        tag: 'span',
                        content: 'ЕвроОбувь в соц.сетях'
                    }, {
                        elem: 'list',
                        tag: 'ul',
                        content: [{
                            elem: 'item',
                            tag: 'li',
                            content: [{
                                elem: 'link',
                                tag: 'a',
                                attrs: {
                                    href: '#'
                                },
                                content: [{
                                    mix: {
                                        block: 'icon-vk'
                                    },
                                    tag: 'i'
                                }]
                            }]
                        }, {
                            elem: 'item',
                            tag: 'li',
                            content: [{
                                elem: 'link',
                                tag: 'a',
                                attrs: {
                                    href: '#'
                                },
                                content: [{
                                    mix: {
                                        block: 'icon-fb'
                                    },
                                    tag: 'i'
                                }]
                            }]
                        }, {
                            elem: 'item',
                            tag: 'li',
                            content: [{
                                elem: 'link',
                                tag: 'a',
                                attrs: {
                                    href: '#'
                                },
                                content: [{
                                    mix: {
                                        block: 'icon-ok'
                                    },
                                    tag: 'i'
                                }]
                            }]
                        }, {
                            elem: 'item',
                            tag: 'li',
                            content: [{
                                elem: 'link',
                                tag: 'a',
                                attrs: {
                                    href: '#'
                                },
                                content: [{
                                    mix: {
                                        block: 'icon-inst'
                                    },
                                    tag: 'i'
                                }]
                            }]
                        }, {
                            elem: 'item',
                            tag: 'li',
                            content: [{
                                elem: 'link',
                                tag: 'a',
                                attrs: {
                                    href: '#'
                                },
                                content: [{
                                    mix: {
                                        block: 'icon-g'
                                    },
                                    tag: 'i'
                                }]
                            }]
                        }]
                    }]
                }]
            }]
        }]
        }, {
            block: 'subfooter',
            content: [{
                mix: [{block: 'container'}, {block: 'clearfix'}],
                content: [{
                    block: 'float-left',
                    content: [{
                        block: 'copyright',
                        tag: 'span',
                        content: '© 2012-2018 «ЕвроОбувь»'
                    }]
                }, {
                    block: 'float-right',
                    content: [{
                        block: 'develop',
                        tag: 'a',
                        attrs: {
                          href: '#'
                        },
                        content: [{
                            block: 'img',
                            src: 'images/iv-logo.png',
                            attrs: {
                                alt: 'iv'
                            }
                        }, 'Разработка сайта']
                    }]
                }]
            }]
        }
    ]
    }
];
