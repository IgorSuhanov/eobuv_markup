module.exports = (data)=>{
    return {
        block: 'product-thumbnail',
        mods: {
            hidden: data.hidden
        },
        attrs: {
            id: data.id
        },
        content: [{
            elem: 'image',
            tag: 'img',
            attrs: {
                src: data.src,
                alt: data.title
            }
        }, {
            elem: 'list',
            tag: 'ul',
            content: [{
                elem: 'tag',
                tag: 'li',
                elemMods: {
                    color: 'green'
                },
                content: [{
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'new'
                }]
            }, {
                elem: 'tag',
                tag: 'li',
                elemMods: {
                    color: 'yellow'
                },
                content: [{
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: 'ХИТ!'
                }]
            }]
        }, {
            elem :'title',
            tag: 'h5',
            content: data.title
        }, {
            elem: 'code',
            tag: 'span',
            content: data.code
        }, {
            elem: 'price-wrapper',
            tag: 'span',
            content: [{
                elem: 'price',
                tag: 'span',
                elemMods: {
                    old: true
                },
                content: data.oldPrice
            }, {
                elem: 'price',
                tag: 'span',
                content: data.price
            }]
        }]
    }
};
