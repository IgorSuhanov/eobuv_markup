module.exports = [
    {
        block: 'header', content: [
        {
            mix: {block: 'container'}, content: [
                {
            elem: 'overhead',
            mix: {
              block: 'row'
            },
            content: [{
                mix: [{
                    block: 'col-xl-2'
                }, {
                    block: 'col-lg-2'
                }, {
                    block: 'col-md-3'
                }, {
                    block: 'col-sm-3'
                }, {
                    block: 'col-2'
                }],
                content: [{
                    block: 'location',
                    tag: 'span',
                    content: [{
                        mix: {
                            block: 'icon-pin'
                        },
                        tag: 'i'
                    }, {
                        elem: 'description',
                        tag: 'span',
                        content: 'Местоположение'
                    }, {
                        elem: 'change',
                        tag: 'a',
                        attrs: {
                            href: '#'
                        },
                        content: 'Волгоград'
                    }]
                }]
            }, {
                mix: [{
                    block: 'col-xl-3'
                }, {
                    block: 'col-lg-3'
                }, {
                    block: 'col-md-4'
                }, {
                    block: 'col-sm-4'
                }, {
                    block: 'col-4'
                }],
                content: [{
                    block: 'phone',
                    mods: {
                      'space-left': true
                    },
                    tag: 'a',
                    attrs: {
                        href: 'tel:88001000324'
                    },
                    content: [{
                        mix: {
                            block: 'icon-phone'
                        },
                        tag: 'i'
                    }, '88001000324', {
                        elem: 'title',
                        tag: 'span',
                        content: 'Звонок по России бесплатный'
                    }]
                }]
            }, {
                mix: [{
                    block: 'col-xl-3'
                }, {
                    block: 'col-lg-4'
                }, {
                    block: 'd-xl-block'
                }, {
                    block: 'd-lg-block'
                }, {
                    block: 'd-md-none'
                }, {
                    block: 'd-sm-none'
                }, {
                    block: '.d-none'
                }],
                content: [{
                    block: 'opening-hours',
                    content: [{
                        mix: {
                            block: 'icon-clock'
                        },
                        tag: 'i'
                    }, {
                        elem: 'time',
                        tag: 'span',
                        content: 'Время работы: 9:00 - 18:00'
                    }, {
                        elem: 'title',
                        tag: 'span',
                        content: 'Без перерыва и выходных'
                    }]
                }]
            }, {
                mix: [{
                    block: 'col-xl-2'
                }, {
                    block: 'col-lg-2'
                }, {
                    block: 'col-md-3'
                }, {
                    block: 'col-sm-3'
                }, {
                    block: 'col-2'
                }],
                content: [{
                    block: 'get-feedback',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: [{
                        mix: {
                            block: 'icon-get-feedback'
                        },
                        tag: 'i'
                    }, {
                        elem: 'title',
                        tag: 'span',
                        content: 'Заказать звонок'
                    }]
                }]
            }, {
                mix: [{
                    block: 'col-xl-2'
                }, {
                    block: 'col-lg-1'
                }, {
                    block: 'col-sm-2'
                }, {
                    block: 'col-2'
                }],
                content: [{
                    block: 'sign-in',
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: [{
                        mix: {
                            block: 'icon-lock'
                        },
                        tag: 'i'
                    }, {
                        elem: 'title',
                        tag: 'span',
                        content: 'Войти'
                    }]
                }]
            }]
        },
            {
            block: 'navbar',
            mix: [{
                block: 'navbar-expand-lg'
            }, {
                block: 'navbar-light'
            }, {
                block: 'row'
            }],
            tag: 'nav',
            content: [
                {
                    elem: 'brand',
                    mix: {
                        block: 'navbar-brand'
                    },
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: [{
                        block: 'logo',
                        tag: 'img',
                        attrs: {
                            src: 'images/logo.png',
                            alt: 'logo'
                        }
                    }, {
                        mix: {
                            block: 'd-block'
                        },
                        tag: 'small',
                        content: 'обувь европейских производителей'
                    }]
                }, {
                    elem: 'toggler',
                    mix: [{
                        block: 'navbar-toggler'
                    }],
                    tag: 'button',
                    attrs: {
                        type: 'button'
                    },
                    content: [{
                        mix: {
                            block: 'navbar-toggler-icon'
                        },
                        tag: 'span'
                    }]
                }, {
                    block: 'navigation',
                    attrs: {
                        id: 'navbarToggler'
                    },
                    content: [{
                        mix: [{
                            block: 'navbar-nav'
                        }],
                        tag: 'ul',
                        content: [{
                            elem: 'item',
                            menuId: 'menu-item1',
                            text: 'Женская'
                        }, {
                            elem: 'item',
                            menuId: 'menu-item2',
                            text: 'Мужская'
                        }, {
                            elem: 'item',
                            menuId: 'menu-item3',
                            text: 'Аксессуары'
                        }, {
                            elem: 'item',
                            text: 'Бренды'
                        }, {
                            elem: 'item',
                            text: 'Акции'
                        }]
                    }]
                }, {
                    block: 'search-wrapper',
                    tag: 'form',
                    content: [{
                        block: 'search',
                        mix: [{
                            block: 'form-inline'
                        }],
                        content: [{
                            block: 'form-control',
                            type: 'search',
                            placeholder: 'Поиск по сайту',
                            ariaLabel: 'Search'
                        }, {
                            elem: 'button',
                            tag: 'button',
                            attrs: {
                                type: 'submit'
                            },
                            content: [{
                                mix: {
                                    block: 'icon-search'
                                },
                                tag: 'i'
                            }]
                        }]
                    }]
                }, {
                    block: 'product-panel',
                    tag: 'ul',
                    content: [{
                        elem: 'item',
                        tag: 'li',
                        content: [{
                            elem: 'link',
                            tag: 'a',
                            attrs: {
                                href: '#'
                            },
                            content: [{
                                elem: 'icon',
                                mix: {
                                    block: 'icon-favorites'
                                },
                                tag: 'i'
                            }, {
                                elem: 'counter',
                                elemMods: {
                                    active: true
                                },
                                tag: 'i',
                                content: '3'
                            }]
                        }]
                    }, {
                        elem: 'item',
                        tag: 'li',
                        content: [{
                            elem: 'link',
                            tag: 'span',
                            content: [{
                                elem: 'icon',
                                mix: {
                                    block: 'icon-cart'
                                },
                                tag: 'i'
                            }, {
                                elem: 'counter',
                                tag: 'i',
                                content: '0'
                            }]
                        }]
                    }]
                },
                {
                    block: 'search-modal',
                    content: [{
                        elem: 'wrapper',
                        content: [{
                            mix: {
                                block: 'row'
                            },
                            content: [{
                                elem: 'column',
                                content: [{
                                    elem: 'heading',
                                    tag: 'h5',
                                    content: 'Товары'
                                }, {
                                    elem: 'item',
                                    tag: 'a',
                                    attrs: {
                                        href: '#'
                                    },
                                    content: [{
                                        elem: 'item-image',
                                        tag: 'span',
                                        content: [{
                                            block: 'img',
                                            src: 'images/item1.png',
                                            attrs: {alt: 'image'}
                                        }]
                                    }, {
                                        elem: 'item-title',
                                        tag: 'span',
                                        content: 'Сапоги COVER'
                                    }, {
                                        elem: 'item-description',
                                        tag: 'small',
                                        content: 'T12743-223'
                                    }]
                                }, {
                                    elem: 'item',
                                    tag: 'a',
                                    attrs: {
                                        href: '#'
                                    },
                                    content: [{
                                        elem: 'item-image',
                                        tag: 'span',
                                        content: [{
                                            block: 'img',
                                            src: 'images/item2.png',
                                            attrs: {alt: 'image'}
                                        }]
                                    }, {
                                        elem: 'item-title',
                                        tag: 'span',
                                        content: 'Сапоги COVER'
                                    }, {
                                        elem: 'item-description',
                                        tag: 'small',
                                        content: 'T12743-223'
                                    }]
                                }, {
                                    elem: 'item',
                                    tag: 'a',
                                    attrs: {
                                        href: '#'
                                    },
                                    content: [{
                                        elem: 'item-image',
                                        tag: 'span',
                                        content: [{
                                            block: 'img',
                                            src: 'images/item3.png',
                                            attrs: {alt: 'image'}
                                        }]
                                    }, {
                                        elem: 'item-title',
                                        tag: 'span',
                                        content: 'Сапоги COVER'
                                    }, {
                                        elem: 'item-description',
                                        tag: 'small',
                                        content: 'T12743-223'
                                    }]
                                }, {
                                    elem: 'item',
                                    tag: 'a',
                                    attrs: {
                                        href: '#'
                                    },
                                    content: [{
                                        elem: 'item-image',
                                        tag: 'span',
                                        content: [{
                                            block: 'img',
                                            src: 'images/item2.png',
                                            attrs: {alt: 'image'}
                                        }]
                                    }, {
                                        elem: 'item-title',
                                        tag: 'span',
                                        content: 'Сапоги COVER'
                                    }, {
                                        elem: 'item-description',
                                        tag: 'small',
                                        content: 'T12743-223'
                                    }]
                                }, {
                                    elem: 'item',
                                    tag: 'a',
                                    attrs: {
                                        href: '#'
                                    },
                                    content: [{
                                        elem: 'item-image',
                                        tag: 'span',
                                        content: [{
                                            block: 'img',
                                            src: 'images/item1.png',
                                            attrs: {alt: 'image'}
                                        }]
                                    }, {
                                        elem: 'item-title',
                                        tag: 'span',
                                        content: 'Сапоги COVER'
                                    }, {
                                        elem: 'item-description',
                                        tag: 'small',
                                        content: 'T12743-223'
                                    }]
                                }]
                            }, {
                                elem: 'column',
                                content: [{
                                    elem: 'section',
                                    content: [{
                                        elem: 'heading',
                                        tag: 'h5',
                                        content: 'Разделы'
                                    }, {
                                        elem: 'compare',
                                        tag: 'a',
                                        attrs: {
                                            href: '#'
                                        },
                                        content: ['Женская обувь', {tag: 'span', content: ' — '}, {
                                            tag: 'strong',
                                            content: [{elem: 'overlap', tag: 'span', content: 'Бот'}, 'ильоны']
                                        }]
                                    }, {
                                        elem: 'compare',
                                        tag: 'a',
                                        attrs: {
                                            href: '#'
                                        },
                                        content: ['Женская обувь', {tag: 'span', content: ' — '}, {
                                            tag: 'strong',
                                            content: [{elem: 'overlap', tag: 'span', content: 'Бот'}, 'инки']
                                        }]
                                    }, {
                                        elem: 'compare',
                                        tag: 'a',
                                        attrs: {
                                            href: '#'
                                        },
                                        content: ['Женская обувь', {tag: 'span', content: ' — '}, {
                                            tag: 'strong',
                                            content: [{elem: 'overlap', tag: 'span', content: 'Бот'}, 'форты']
                                        }]
                                    }]
                                }, {
                                    elem: 'section',
                                    content: [{
                                        elem: 'heading',
                                        tag: 'h5',
                                        content: 'Бренды'
                                    }, {
                                        elem: 'link',
                                        tag: 'a',
                                        attrs: {
                                            href: '#'
                                        },
                                        content: 'Cover'
                                    }, {
                                        elem: 'link',
                                        tag: 'a',
                                        attrs: {
                                            href: '#'
                                        },
                                        content: 'Rieker'
                                    }, {
                                        elem: 'link',
                                        tag: 'a',
                                        attrs: {
                                            href: '#'
                                        },
                                        content: 'Tamaris'
                                    }]
                                }]
                            }]
                        }, {
                            mix: [{
                                block: 'text-center'
                            }, {
                                block: 'mt-3'
                            }],
                            content: [{
                                elem: 'view-all',
                                tag: 'a',
                                attrs: {
                                    href: '#',
                                    role: 'button'
                                },
                                mix: [{
                                    block: 'btn-danger'
                                }, {
                                    block: 'btn-sm'
                                }, {
                                    block: 'd-inline-block'
                                }],
                                content: 'Все результаты'
                            }]
                        }]
                    }]
                },
                {
                    block: 'drop-menu',
                    mix: {
                        block: 'row'
                    },
                    attrs: {
                        'data-dropdown': 'menu-item1'
                    },
                    content: [{
                        elem: 'column',
                        content: [{
                            elem: 'wrapper',
                            content: [{
                                mix: {
                                    block: 'row'
                                },
                                content: [{
                                    mix: [{
                                        block: 'col-xl-6',
                                    }, {
                                        block: 'col-lg-6',
                                    }, {
                                        block: 'col-md-6',
                                    }, {
                                        block: 'col-sm-6',
                                    }, {
                                        block: 'col-6',
                                    }],
                                    content: [{
                                        tag: 'strong',
                                        content: 'Категории 1'
                                    }, {
                                        elem: 'link-wrapper',
                                        content: [{
                                            elem: 'link-column',
                                            content: [{
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#',
                                                    'data-product': 'item1'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Балетки'
                                            }, {
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#',
                                                    'data-product': 'item2'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Босоножки'
                                            }, {
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Ботильоны'
                                            }, {
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#',
                                                    'data-product': 'item3'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Ботинки'
                                            }, {
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Домашняя обувь'
                                            }, {
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#',
                                                    'data-product': 'item4'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Дутики и луноходы'
                                            }, {
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Кроссовки и кеды'
                                            }, {
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#',
                                                    'data-product': 'item5'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Мокасины и топсайдеры'
                                            }, {
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Обувь с увеличенной полнотой'
                                            }]
                                        }, {
                                            elem: 'link-column',
                                            content: [{
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#',
                                                    'data-product': 'item6'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Домашняя обувь'
                                            }, {
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#',
                                                    'data-product': 'item7'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Дутики и луноходы'
                                            }, {
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#',
                                                    'data-product': 'item8'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Кроссовки и кеды'
                                            }, {
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Мокасины и топсайдеры'
                                            }, {
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Обувь с увеличенной полнотой'
                                            }]
                                        }]
                                    }]
                                }, {
                                    mix: [{
                                        block: 'col-xl-3'
                                    }, {
                                        block: 'col-lg-3'
                                    }, {
                                        block: 'col-md-3'
                                    }, {
                                        block: 'col-sm-3'
                                    }, {
                                        block: 'col-3'
                                    }],
                                    content: [{
                                        tag: 'strong',
                                        content: 'Коллекция'
                                    }, {
                                        elem: 'link-wrapper',
                                        content: [{
                                            elem: 'link-column',
                                            content: [{
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Зима 2018'
                                            }, {
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Весна 2018'
                                            }, {
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Лето 2018'
                                            }, {
                                                elem: 'link',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#'
                                                },
                                                mix: {
                                                    block: 'dropdown-item'
                                                },
                                                content: 'Осень 2018'
                                            }]
                                        }]
                                    }]
                                }, {
                                    mix: [{
                                        block: 'col-xl-3'
                                    }, {
                                        block: 'col-lg-3'
                                    }, {
                                        block: 'col-md-3'
                                    }, {
                                        block: 'col-sm-3'
                                    }, {
                                        block: 'col-3'
                                    }],
                                    content: [require('./product-thumbnail.bemjson')({
                                        id: 'item1',
                                        src: 'images/product/product1.png',
                                        title: 'Сапоги Tamaris',
                                        code: 'T12743-223',
                                        oldPrice: '5490 руб.',
                                        price: '3590 руб.',
                                        hidden: true
                                    }), require('./product-thumbnail.bemjson')({
                                        id: 'item2',
                                        src: 'images/product/product2.png',
                                        title: 'Сапоги Tamaris',
                                        code: 'T12743-223',
                                        oldPrice: '5490 руб.',
                                        price: '3590 руб.',
                                        hidden: true
                                    }), require('./product-thumbnail.bemjson')({
                                        id: 'item3',
                                        src: 'images/product/product3.png',
                                        title: 'Сапоги COVER',
                                        code: 'T12743-223',
                                        oldPrice: '5490 руб.',
                                        price: '3590 руб.',
                                        hidden: true
                                    }), require('./product-thumbnail.bemjson')({
                                        id: 'item4',
                                        src: 'images/product/product4.png',
                                        title: 'Сапоги Tamaris',
                                        code: 'T12743-223',
                                        oldPrice: '5490 руб.',
                                        price: '3590 руб.',
                                        hidden: true
                                    }), require('./product-thumbnail.bemjson')({
                                        id: 'item5',
                                        src: 'images/product/product5.png',
                                        title: 'Сапоги Rieker',
                                        code: 'T12743-223',
                                        oldPrice: '5490 руб.',
                                        price: '3590 руб.',
                                        hidden: true
                                    }), require('./product-thumbnail.bemjson')({
                                        id: 'item6',
                                        src: 'images/product/product2.png',
                                        title: 'Сапоги Rieker',
                                        code: 'T12743-223',
                                        oldPrice: '5490 руб.',
                                        price: '3590 руб.',
                                        hidden: true
                                    }), require('./product-thumbnail.bemjson')({
                                        id: 'item7',
                                        src: 'images/product/product3.png',
                                        title: 'Сапоги Rieker',
                                        code: 'T12743-223',
                                        oldPrice: '5490 руб.',
                                        price: '3590 руб.',
                                        hidden: true
                                    }), require('./product-thumbnail.bemjson')({
                                        id: 'item8',
                                        src: 'images/product/product4.png',
                                        title: 'Сапоги Rieker',
                                        code: 'T12743-223',
                                        oldPrice: '5490 руб.',
                                        price: '3590 руб.',
                                        hidden: true
                                    })]
                                }]
                            }]
                        }]
                    }]
                }
            ]
        },
            {
                block: 'aside-menu',
                content: [{
                    elem: 'wrapper',
                    tag: 'aside',
                    content: [{
                        elem: 'list',
                        tag: 'nav',
                        content: [{
                            content: [{
                                elem: 'link',
                                tag: 'a',
                                attrs: {
                                    href: '#'
                                },
                                elemMods: {
                                    expanded: true
                                },
                                content: 'Женская'
                            }, {
                                elem: 'inner-content',
                                content: [{
                                    elem: 'back',
                                    tag: 'a',
                                    attrs: {
                                        href: '#'
                                    },
                                    content: [{
                                        tag: 'i',
                                        mix: {
                                            block: 'icon-prev'
                                        }
                                    }, 'Назад']
                                }, {
                                    content: [{
                                        elem: 'link',
                                        tag: 'a',
                                        attrs: {
                                            href: '#'
                                        },
                                        elemMods: {
                                            expanded: true
                                        },
                                        content: 'Категории'
                                    }, {
                                        elem: 'inner-content',
                                        content: [{
                                            elem: 'back',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: [{
                                                tag: 'i',
                                                mix: {
                                                    block: 'icon-prev'
                                                }
                                            }, 'Назад']
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Балетки'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Босоножки'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Ботильоны'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Ботинки'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Домашняя обувь'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Дутики и луноходы'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Кроссовки и кеды'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Мокасины и топсайдеры'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Обувь с увеличенной полнотой'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Домашняя обувь'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Дутики и луноходы'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Кроссовки и кеды'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Мокасины и топсайдеры'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Обувь с увеличенной полнотой'
                                        }]
                                    }]
                                }, {
                                    content: [{
                                        elem: 'link',
                                        tag: 'a',
                                        attrs: {
                                            href: '#'
                                        },
                                        elemMods: {
                                            expanded: true
                                        },
                                        content: 'Коллекция'
                                    }, {
                                        elem: 'inner-content',
                                        content: [{
                                            elem: 'back',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: [{
                                                tag: 'i',
                                                mix: {
                                                    block: 'icon-prev'
                                                }
                                            }, 'Назад']
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Зима 2018'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Весна 2018'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Лето 2018'
                                        }, {
                                            elem: 'link',
                                            tag: 'a',
                                            attrs: {
                                                href: '#'
                                            },
                                            content: 'Осень 2018'
                                        }]
                                    }]
                                }]
                            }]
                        }, {
                            content: [{
                                elem: 'link',
                                tag: 'a',
                                attrs: {
                                    href: '#'
                                },
                                elemMods: {
                                    expanded: true
                                },
                                content: 'Мужская'
                            }]
                        }, {
                            content: [{
                                elem: 'link',
                                tag: 'a',
                                attrs: {
                                    href: '#'
                                },
                                elemMods: {
                                    expanded: true
                                },
                                content: 'Аксессуары'
                            }]
                        }, {
                            content: [{
                                elem: 'link',
                                tag: 'a',
                                attrs: {
                                    href: '#'
                                },
                                elemMods: {
                                    expanded: true
                                },
                                content: 'Бренды'
                            }]
                        }, {
                            content: [{
                                elem: 'link',
                                tag: 'a',
                                attrs: {
                                    href: '#'
                                },
                                elemMods: {
                                    expanded: true
                                },
                                content: 'Акции'
                            }]
                        }]
                    }, {
                        elem: 'mobile-content',
                        content: [{
                            block: 'location',
                            tag: 'span',
                            content: [{
                                mix: {
                                    block: 'icon-pin'
                                },
                                tag: 'i'
                            }, {
                                elem: 'description',
                                tag: 'span',
                                content: 'Местоположение'
                            }, {
                                elem: 'change',
                                tag: 'a',
                                attrs: {
                                    href: '#'
                                },
                                content: 'Волгоград'
                            }]
                        }, {
                            block: 'phone',
                            mods: {
                                'space-left': true
                            },
                            tag: 'a',
                            attrs: {
                                href: 'tel:88001000324'
                            },
                            content: [{
                                mix: {
                                    block: 'icon-phone'
                                },
                                tag: 'i'
                            }, '88001000324', {
                                elem: 'title',
                                tag: 'span',
                                content: 'Звонок по России бесплатный'
                            }]
                        }, {
                            block: 'search-wrapper',
                            tag: 'form',
                            content: [{
                                block: 'search',
                                mix: [{
                                    block: 'form-inline'
                                }],
                                content: [{
                                    block: 'form-control',
                                    type: 'search',
                                    placeholder: 'Поиск по сайту',
                                    ariaLabel: 'Search'
                                }, {
                                    elem: 'button',
                                    tag: 'button',
                                    attrs: {
                                        type: 'submit'
                                    },
                                    content: [{
                                        mix: {
                                            block: 'icon-search'
                                        },
                                        tag: 'i'
                                    }]
                                }]
                            }]
                        }, {
                            block: 'sign-in',
                            tag: 'a',
                            attrs: {
                                href: '#'
                            },
                            content: [{
                                mix: {
                                    block: 'icon-lock'
                                },
                                tag: 'i'
                            }, {
                                elem: 'title',
                                tag: 'span',
                                content: 'Войти'
                            }]
                        }]
                    }]
                }]
            }
            ]
        }
    ]
    }
];
