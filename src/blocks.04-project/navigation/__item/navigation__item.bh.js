module.exports = function (bh) {
    bh.match('navigation__item', function (ctx, json) {
        ctx.tag('li');
        ctx.mix({
            block: 'nav-item'
        });
        ctx.content([{
            elem: 'link',
            mix: [{
                block: 'nav-link'
            }],
            tag: 'a',
            attrs: {
                href: '#',
                'data-menu-id': json.menuId
            },
            content: json.text
        }], true);
    });
};


