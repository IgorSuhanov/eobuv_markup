({
    mustDeps: []
}, {
    shouldDeps: [{
        block: 'aside-menu',
        elem: 'inner-content',
        elemMod: 'open'
    }]
});