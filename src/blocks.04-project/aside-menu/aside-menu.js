$(function () {
   var button = $('.navbar__toggler'),
       menu = $('.aside-menu');

   button.on('click', function () {
       menu.addClass('aside-menu_open');
   });

    menu.on('click', function (e) {
        var target = $(e.target),
            targetCheck = target.hasClass('aside-menu__wrapper') || target.parents().hasClass('aside-menu__wrapper');

        if (!targetCheck) {
            $(this).removeClass('aside-menu_open');
        }
    });
});