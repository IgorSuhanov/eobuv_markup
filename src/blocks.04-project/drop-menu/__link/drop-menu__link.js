$(function () {
   var links = $('.drop-menu__link'),
       thumbnail = $('.product-thumbnail'),
       setDelay = false;

    links.on('mouseover', function (e) {
        if ($(this).attr('data-product')) {
            $(this).data('f', true);

            if (setDelay) {
                clearTimeout(setDelay);
            }

            setDelay = setTimeout(function () {
                if($(this).data('f')){
                    $(this).data('f', false);
                    thumbnail.hide();
                    $('#' + $(this).attr('data-product')).css({
                        display: 'flex'
                    });
                }

                clearTimeout(setDelay);

            }.bind(this), 500);
        }
    });

    links.on('mouseout', function () {
        $(this).data('f', false);
    });
});