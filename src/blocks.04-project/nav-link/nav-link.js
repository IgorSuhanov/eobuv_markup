$(function () {
    var dropdown = $('.nav-link, .drop-menu'),
        menu = $('.drop-menu');

    dropdown.on('mouseover', function () {
        if (this.className.indexOf('nav-link') >= 0) {
            $('[data-dropdown=' + $(this).attr('data-menu-id') + ']').show();
        } else if (this.className.indexOf('drop-menu') >= 0) {
            $(this).show();
        }
    });

    dropdown.on('mouseout', function () {
        menu.hide();
    });
});