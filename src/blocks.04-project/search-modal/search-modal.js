$(function () {
    var $input = $('.search > input'),
        $modal = $('.search-modal'),
        $search = $('.search');

    $input.on('input', function () {
        if ($(this).val().length >= 3) {
            $modal
                .addClass('search-modal_expanded')
                .fadeIn();
            $search
                .addClass('search_expanded');
        } else {
            $modal
                .removeClass('search-modal_expanded')
                .fadeOut();
            $search
                .removeClass('search_expanded');
        }
    });

    $('body').on('click',  function (e) {
        var target = $(e.target),
            checkModal = !target.hasClass('search-modal__wrapper'),
            checkModalParents = target.parents('.search-modal__wrapper').length !== 1,
            checkSearch = !target.hasClass('search'),
            checkSearchParents = target.parents('.search').length !== 1;
        if (checkModal && checkModalParents && checkSearch && checkSearchParents) {
            $modal
                .removeClass('search-modal_expanded')
                .hide();

            $search
                .removeClass('search_expanded');

            $input.val('');
        }
    });
});