import lazysizes from 'lazysizes';

lazySizesConfig = lazySizesConfig || {};
lazySizesConfig.init = false;

lazysizes.init();