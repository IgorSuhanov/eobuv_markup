module.exports = function(bh) {
    bh.match('figure', function(ctx, json) {
        ctx.tag('figure');
        ctx.content([{
            block: 'img',
            src: json.src,
            attrs: {
                alt: json.alt
            }
        }, {
            elem: 'caption',
            tag: 'figcaption',
            content: json.caption
        }], true);
    });
};