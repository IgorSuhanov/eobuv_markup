module.exports = function(bh) {
    bh.match('figure__caption', function(ctx, json) {
        ctx.tag('figcaption');
        ctx.content([{
            tag: false,
            content: ctx.content()
        }], true);
    });
};