({
    mustDeps: []
}, {
    shouldDeps: [{
        block: 'img'
    }, {
        elem: 'caption'
    }]
});
