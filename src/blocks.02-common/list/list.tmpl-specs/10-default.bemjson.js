module.exports = [
    {block: 'list', content: [
        'Пункт 1',
        'Пункт 2',
        {block: 'list', content: [
            'Пункт 1',
            'Пункт 2',
            'Пункт 3'
        ]},
        'Пункт 3'
    ]}
]
