({
    mustDeps: []
}, {
    shouldDeps: [{
        block: 'font-exo-2'
    }, {
        block: 'font-rubik'
    }, {
        block: 'eobuv-icons'
    }, {
        block: 'swiper'
    }]
});
