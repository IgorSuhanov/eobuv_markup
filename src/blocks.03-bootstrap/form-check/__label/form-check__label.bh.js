module.exports = function(bh) {
    bh.match('form-check__label', function(ctx, json) {
        ctx.tag('label');
        ctx.cls('form-check-label');
        ctx.attrs({
            for: ctx.tParam('ID')
        });
    });
};