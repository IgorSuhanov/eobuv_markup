({
    mustDeps: []
}, {
    shouldDeps: [{
        block: 'form-check',
    }, {
        block: 'form-check',
        elem: 'input',
    }, {
        block: 'form-check',
        elem: 'label'
    }]
});
