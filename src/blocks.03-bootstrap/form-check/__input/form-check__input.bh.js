module.exports = function(bh) {
    bh.match('form-check__input', function(ctx, json) {
        ctx
            .mod('styled', true)
            .tag('input')
            .cls('form-check-input')
            .attrs({
                type: json.type || 'checkbox',
                value: json.val || ctx.generateId(),
                name: json.name || ctx.generateId(),
                id: ctx.tParam('ID') , //TODO: ctx.generateId() но так как label должен иметь тот же for нужен еще ctx.tParam() спроси раскажу подробнее
                disabled: json.disabled
            })
            .content(false, true);

            return [
                ctx.json(),
                ctx.mod('styled') && {elem: 'dot', tag: 'span'}
            ]
    });
};