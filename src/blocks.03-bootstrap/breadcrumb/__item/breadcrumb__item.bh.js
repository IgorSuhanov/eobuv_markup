module.exports = function(bh) {
    bh.match('breadcrumb__item', function(ctx, json) {
        ctx.tag('li');
        ctx.content([{
            tag: false,
            content: ctx.content()
        }], true);
    });
};
/*
Я бы написал примерно так:
module.exports = function (bh) {
    bh.match('breadcrumb', function (ctx, json) {
        ctx.tag('ol').content(
            ctx.content().map((item)=>{
                if(typeof item !== 'string') return item;
                return {elem: 'li', content: item}
            }),
            true
        );
    });
};
*/

