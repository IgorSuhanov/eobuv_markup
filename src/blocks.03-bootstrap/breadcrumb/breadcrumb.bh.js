module.exports = function(bh) {
    bh.match('breadcrumb', function(ctx, json) {
        ctx.tag('ol');
        ctx.content([json.items.map(function (item) {
            return {
                elem: 'item',
                cls: 'breadcrumb-item',
                attrs: {
                    'aria-current': item.active ? item.active : false
                },
                content: [{
                    tag: 'a',
                    attrs: {
                        href: '#'
                    },
                    content: item.name
                }]
            };
        })], true);

        return {
            tag: 'nav',
            attrs: {
                'aria-label': 'breadcrumb'
            },
            content: ctx.json()
        }
    });
};


    