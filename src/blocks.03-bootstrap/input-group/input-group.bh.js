module.exports = function(bh) {
    bh.match('input-group', function(ctx, json) {
        ctx.content([{
            block: 'form-control',
            type: json.type,
            placeholder: json.placeholder,
            content: json.val,
            required: json.required
        }, {
            block: 'valid-feedback',
            content: json.validFeedback
        }, {
            block: 'invalid-feedback',
            content: json.invalidFeedback
        }], true);
    });
};


